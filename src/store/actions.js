export default {
  updateLoading({ commit }, payload) {
    commit('setLoading', payload)
  }
}