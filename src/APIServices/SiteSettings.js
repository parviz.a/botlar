import axios from 'axios';
import { BASE_URL } from './Variables'
const API_URL = `${BASE_URL}`;

export const SiteSettings = {
    /*
    * Site settings
    */
    getSettings() {
        const url = `${API_URL}/site`;
        return axios.get(url);
    }
}