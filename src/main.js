import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import "./assets/custom.scss";
import VuePlyr from 'vue-plyr'
import 'vue-plyr/dist/vue-plyr.css'


Vue.config.productionTip = false
Vue.use(VuePlyr, {
  plyr: {}
})
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
